package spacebase.world;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.TagManager;
import spacebase.world.components.ComponentFactory;
import spacebase.world.components.MapComponent;
import spacebase.world.components.ViewPortComponent;
import spacebase.world.util.SimpleRectangle;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 30/09/2012
 * Time: 00:32
 * To change this template use File | Settings | File Templates.
 */
public class MapGenerator
{
    private EntityFactory playerEntityFactory;
    private EntityFactory pirateEntityFactory;
    private EntityFactory neutralEntityFactory;

    private SimpleRectangle worldSize;
    private SimpleRectangle viewPort;

    private World world;
    private ComponentFactory componentFactory;


    public void generate(int seed)
    {
        MapComponent map = createMap(
                seed,
                worldSize.getWidth(),
                worldSize.getHeight()
        ).getComponent(MapComponent.class);

        for(int x = 0; x < map.getWidth(); x++)
        {
            for(int y = 0; y < map.getHeight(); y++)
            {
                neutralEntityFactory.createTerrain(x, y);
            }

        }


        createViewPort(
                viewPort.getX(),
                viewPort.getY(),
                viewPort.getWidth(),
                viewPort.getHeight()
        );


        // players
        int[] player = new int[]{map.getWidth()/2, map.getHeight()/2};
        int[] airGenerator = new int[]{player[0]+1, player[1]};

        int[] baseTopLeft = new int[]{player[0]-1, player[1]-1};
        int[] baseBottomRight = new int[]{baseTopLeft[0]+4, baseTopLeft[1]+4};

        //builds base
        // top line
        for(int y : new int[]{baseTopLeft[1], baseBottomRight[1]})
        {
            for(int x = baseTopLeft[0]; x <= baseBottomRight[0]; x++)
            {
                playerEntityFactory.createWall(x, y);
            }
        }

        for(int x : new int[]{baseTopLeft[0], baseBottomRight[0]})
        {
            for(int y = baseTopLeft[1]+1; y <= baseBottomRight[1]-1; y++)
            {
                playerEntityFactory.createWall(x, y);
            }
        }


        playerEntityFactory.createAirGenerator(airGenerator[0], airGenerator[1]);
        playerEntityFactory.createPlayer(player[0], player[1]);

    }


    private Entity createMap(long seed, int width, int height)
    {
        Entity entity = world.createEntity();

        MapComponent mapComponent = componentFactory.createMapComponent();
        mapComponent.setWidth(width);
        mapComponent.setHeight(height);
        entity.addComponent(mapComponent);

        world.getManager(TagManager.class).register(Entities.MAP, entity);

        entity.addToWorld();

        return entity;
    }

    private Entity createViewPort(float x, float y, float width, float height)
    {
        Entity entity = world.createEntity();

        ViewPortComponent viewPortComponent = componentFactory.createViewPortComponent();
        viewPortComponent.setMicroX(x);
        viewPortComponent.setMicroY(y);
        viewPortComponent.setMicroWidth(width);
        viewPortComponent.setMicroHeight(height);
        entity.addComponent(viewPortComponent);

        world.getManager(TagManager.class).register(Entities.VIEW_PORT, entity);

        entity.addToWorld();

        return entity;
    }


    public void setPlayerEntityFactory(EntityFactory playerEntityFactory) {
        this.playerEntityFactory = playerEntityFactory;
    }

    public void setPirateEntityFactory(EntityFactory pirateEntityFactory) {
        this.pirateEntityFactory = pirateEntityFactory;
    }

    public void setNeutralEntityFactory(EntityFactory neutralEntityFactory) {
        this.neutralEntityFactory = neutralEntityFactory;
    }

    public void setWorldSize(SimpleRectangle worldSize) {
        this.worldSize = worldSize;
    }

    public void setViewPort(SimpleRectangle viewPort) {
        this.viewPort = viewPort;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public void setComponentFactory(ComponentFactory componentFactory) {
        this.componentFactory = componentFactory;
    }
}
