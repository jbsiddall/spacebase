package spacebase.world;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 02/10/2012
 * Time: 20:24
 * To change this template use File | Settings | File Templates.
 */
public class GameException extends RuntimeException
{
    public GameException(String s, Object... args) {
        super(String.format(s, args));
    }

    public GameException(Throwable throwable) {
        super(throwable);
    }
}
