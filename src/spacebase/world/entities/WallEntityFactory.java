package spacebase.world.entities;

import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.managers.PlayerManager;
import spacebase.world.Direction;
import spacebase.world.Entities;
import spacebase.world.components.AirFacilitatorComponent;
import spacebase.world.components.PositionComponent;
import spacebase.world.components.SpatialFormComponent;


public class WallEntityFactory extends SpecificEntityFactory
{

    @Override
    public boolean canCreateEntity(int x, int y, Direction direction) {
        return true;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected Entity createEntity(int x, int y, Direction direction) {

        Entity entity = world.createEntity();

        PositionComponent positionComponent = componentFactory.createPositionComponent();
        positionComponent.setX(x);
        positionComponent.setY(y);
        positionComponent.setDirection(direction);
        entity.addComponent(positionComponent);

        AirFacilitatorComponent airFacilitatorComponent = componentFactory.createAirFacilitatorComponent();
        airFacilitatorComponent.setPorous(0.0);
        airFacilitatorComponent.setAirLevel(0);
        airFacilitatorComponent.setPriority(5);
        entity.addComponent(airFacilitatorComponent);

        SpatialFormComponent spatialFormComponent = componentFactory.createSpatialFormComponent();
        spatialFormComponent.setSpatialFormFile(Entities.WALL);
        entity.addComponent(spatialFormComponent);

        world.getManager(PlayerManager.class).setPlayer(entity, owner);
        world.getManager(GroupManager.class).add(entity, Entities.WALL);
        world.getManager(GroupManager.class).add(entity, Entities.SOLID);

        entity.addToWorld();

        return entity;
    }

    @Override
    protected void prepareLocation(int x, int y, Direction direction) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected void forcePrepareLocation(int x, int y, Direction direction) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
