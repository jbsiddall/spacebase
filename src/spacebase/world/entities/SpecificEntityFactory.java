package spacebase.world.entities;

import com.artemis.Entity;
import com.artemis.World;
import spacebase.world.Direction;
import spacebase.world.GameException;
import spacebase.world.components.ComponentFactory;


public abstract class SpecificEntityFactory
{
    protected World world;
    protected String owner;
    protected ComponentFactory componentFactory;

    /**
     * creates the entity.
     * if force is true, deletes any mutually exclusive entities.
     *
     * @param x
     * @param y
     * @param direction
     * @param force
     * @return entity created
     * @throws GameException if canCreateEntity returns false
     */
    public Entity createEntity(int x, int y, Direction direction, boolean force) throws GameException
    {
        if(canCreateEntity(x, y, direction))
        {
            prepareLocation(x, y, direction);
            return createEntity(x, y, direction);
        }
        else if(force)
        {
            forcePrepareLocation(x, y, direction);
            return createEntity(x, y, direction);
        }
        else
        {
            throw new GameException("can't create entity %s at (%d, %d, %s)", this.getClass().getSimpleName(), x, y, direction);
        }
    }


    /**
     * if true the entity can be placed at (x,y) with direction.
     *
     * for example:
     * placing a wall on terrain returns true,
     * however the terrain will be deleted upon creation.
     *
     * placing an air generator on wall returns false,
     * since neither can be deleted passively.
     *
     * @param x
     * @param y
     * @param direction
     * @return
     */
    public abstract boolean canCreateEntity(int x, int y, Direction direction);


    public void setWorld(World world) {
        this.world = world;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setComponentFactory(ComponentFactory componentFactory) {
        this.componentFactory = componentFactory;
    }

    protected abstract Entity createEntity(int x, int y, Direction direction);

    protected abstract void prepareLocation(int x, int y, Direction direction);

    protected abstract void forcePrepareLocation(int x, int y, Direction direction);

}
