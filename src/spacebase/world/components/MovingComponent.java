package spacebase.world.components;

import com.artemis.Component;
import spacebase.world.Direction;


public class MovingComponent extends Component
{
    Direction direction;

    public MovingComponent() {
    }

    public MovingComponent(Direction direction) {
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
