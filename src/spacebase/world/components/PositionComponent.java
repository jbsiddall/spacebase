package spacebase.world.components;

import com.artemis.Component;
import spacebase.world.Direction;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 28/09/2012
 * Time: 17:18
 * To change this template use File | Settings | File Templates.
 */
public class PositionComponent extends Component
{
    private int x;
    private int y;
    private Direction direction;

    public void move(int dx, int dy)
    {
        setX(getX()+dx);
        setY(getY()+dy);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
