package spacebase.world.components;

import com.artemis.Component;

public class  ViewPortComponent extends Component
{

    private float microX;
    private float microY;
    private float microWidth;
    private float microHeight;

    public ViewPortComponent() {
    }

    public ViewPortComponent(float microX, float microY, float microWidth, float microHeight) {
        this.microX = microX;
        this.microY = microY;
        this.microWidth = microWidth;
        this.microHeight = microHeight;
    }

    public void move(float dx, float dy)
    {
        microX += dx;
        microY += dy;
    }

    public void resize(float scale)
    {
        microWidth *= scale;
        microHeight *= scale;
    }

    public void zoomOut(float delta)
    {
        microWidth += delta;
        microHeight += delta;

        microX -= delta/2;
        microY -= delta/2;
    }

    public void zoomIn(float delta)
    {
        microWidth -= delta;
        microHeight -= delta;

        microX += delta/2;
        microY += delta/2;
    }

    public void zoom(float delta)
    {
        if(delta > 0)
        {
            zoomIn(delta);
        }

        if(delta < 0)
        {
            zoomOut(-delta);
        }
    }

    public float getMicroX() {
        return microX;
    }

    public void setMicroX(float microX) {
        this.microX = microX;
    }

    public float getMicroY() {
        return microY;
    }

    public void setMicroY(float microY) {
        this.microY = microY;
    }

    public float getMicroWidth() {
        return microWidth;
    }

    public void setMicroWidth(float microWidth) {
        this.microWidth = microWidth;
    }

    public float getMicroHeight() {
        return microHeight;
    }

    public void setMicroHeight(float microHeight) {
        this.microHeight = microHeight;
    }


    @Override
    public String toString() {
        return "ViewPortComponent{" +
                "microX=" + microX +
                ", microY=" + microY +
                ", microWidth=" + microWidth +
                ", microHeight=" + microHeight +
                '}';
    }
}
