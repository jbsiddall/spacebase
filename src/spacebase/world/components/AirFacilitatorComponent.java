package spacebase.world.components;

import com.artemis.Component;
import spacebase.world.Direction;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 28/09/2012
 * Time: 01:26
 * To change this template use File | Settings | File Templates.
 */
public class AirFacilitatorComponent extends Component
{
    public static final int PRIORITY_MIN = 0;
    public static final int PRIORITY_MAX = 10;


    // if 1.0, allows air to pass through it at maximum, if 0, is air tight.
    private double porous;

    // 1.0 is maximum air level, 0.0 is minimum air level
    private double airLevel;

    private int priority;

    public AirFacilitatorComponent() {
    }

    public AirFacilitatorComponent(double porous, double airLevel, int priority) {
        this.porous = porous;
        this.airLevel = airLevel;
        setPriority(priority);
    }

    public double getPorous() {
        return porous;
    }

    public void setPorous(double porous) {
        this.porous = porous;
    }

    public double getAirLevel() {
        return airLevel;
    }

    public void setAirLevel(double airLevel) {
        this.airLevel = airLevel;
        fixAirLevelRange();
    }

    public void addAir(double delta)
    {
        this.airLevel += delta;
        fixAirLevelRange();
    }

    private void fixAirLevelRange()
    {
        if(this.airLevel > 1.0)
            this.airLevel = 1.0;
        if(this.airLevel < 0.0)
            this.airLevel = 0.0;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        if(priority < PRIORITY_MIN) throw new IllegalArgumentException("priority too low: " + priority);
        if(priority > PRIORITY_MAX) throw new IllegalArgumentException("priority too high: " + priority);
        this.priority = priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AirFacilitatorComponent)) return false;

        AirFacilitatorComponent that = (AirFacilitatorComponent) o;

        if (Double.compare(that.airLevel, airLevel) != 0) return false;
        if (Double.compare(that.porous, porous) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = porous != +0.0d ? Double.doubleToLongBits(porous) : 0L;
        result = (int) (temp ^ (temp >>> 32));
        temp = airLevel != +0.0d ? Double.doubleToLongBits(airLevel) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "AirFacilitatorComponent{" +
                "porous=" + porous +
                ", airLevel=" + airLevel +
                '}';
    }
}
