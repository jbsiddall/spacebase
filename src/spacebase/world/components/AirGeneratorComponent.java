package spacebase.world.components;

import com.artemis.Component;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 29/09/2012
 * Time: 22:45
 * To change this template use File | Settings | File Templates.
 */
public class AirGeneratorComponent extends Component
{
    private double generationAmount;

    public AirGeneratorComponent() {
    }

    public AirGeneratorComponent(double generationAmount) {
        this.generationAmount = generationAmount;
    }

    public double getGenerationAmount() {
        return generationAmount;
    }

    public void setGenerationAmount(double generationAmount) {
        this.generationAmount = generationAmount;
    }
}
