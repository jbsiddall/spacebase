package spacebase.world.components;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 29/09/2012
 * Time: 14:55
 * To change this template use File | Settings | File Templates.
 */
public interface ComponentFactory
{
    public AirFacilitatorComponent createAirFacilitatorComponent();

    public AirGeneratorComponent createAirGeneratorComponent();

    public MapComponent createMapComponent();

    public MovingComponent createMovingComponent();

    public PlayerComponent createPlayerComponent();

    public PositionComponent createPositionComponent();

    public SpatialFormComponent createSpatialFormComponent();

    public ViewPortComponent createViewPortComponent();

}
