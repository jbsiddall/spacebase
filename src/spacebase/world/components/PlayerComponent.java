package spacebase.world.components;

import com.artemis.Component;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 30/09/2012
 * Time: 02:22
 * To change this template use File | Settings | File Templates.
 */
public class PlayerComponent extends Component {
    private int health;

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }


}
