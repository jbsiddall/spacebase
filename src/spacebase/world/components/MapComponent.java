package spacebase.world.components;

import com.artemis.Component;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 28/09/2012
 * Time: 19:39
 * To change this template use File | Settings | File Templates.
 */
public class MapComponent extends Component
{
    private int width;
    private int height;

    public MapComponent() {
    }

    public MapComponent(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
