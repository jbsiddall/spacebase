package spacebase.world.components;

import com.artemis.Component;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 28/09/2012
 * Time: 18:10
 * To change this template use File | Settings | File Templates.
 */
public class SpatialFormComponent extends Component
{
    private String spatialFormFile;

    public SpatialFormComponent() {
    }

    public SpatialFormComponent(String spatialFormFile) {
        this.spatialFormFile = spatialFormFile;
    }

    public String getSpatialFormFile() {
        return spatialFormFile;
    }

    public void setSpatialFormFile(String spatialFormFile) {
        this.spatialFormFile = spatialFormFile;
    }
}
