package spacebase.world.spatials;


import org.newdawn.slick.Graphics;

import com.artemis.Entity;
import com.artemis.World;
import org.newdawn.slick.Image;
import spacebase.world.Direction;
import spacebase.world.ImageLoader;
import spacebase.world.util.SimpleRectangle;

public abstract class Spatial {

    public static final int DEPTH_MIN = 0;
    public static final int DEPTH_MAX = 10;

    protected World world;
    protected Entity owner;

    private int depth;

    protected ImageLoader imageLoader;

    public abstract void initalize();

    public abstract void render(Graphics g);


    public void setWorld(World world) {
        this.world = world;
    }

    public void setOwner(Entity owner) {
        this.owner = owner;
    }

    public int getDepth() {
        return depth;
    }



    public void setDepth(int depth) {
        if(depth < DEPTH_MIN) throw new IllegalArgumentException("depth too small: " + depth);
        if(depth > DEPTH_MAX) throw new IllegalArgumentException("depth too high: " + depth);
        this.depth = depth;
    }

    public void setImageLoader(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    protected void rotateImage(Image image, Direction direction)
    {

        switch(direction)
        {
            case NORTH: break;
            case EAST: image.setRotation(90); break;
            case SOUTH: image.setRotation(180); break;
            case WEST: image.setRotation(270); break;
        }
    }
}

