package spacebase.world.spatials;

import com.artemis.ComponentMapper;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import spacebase.world.Entities;
import spacebase.world.components.AirFacilitatorComponent;
import spacebase.world.components.PositionComponent;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 29/09/2012
 * Time: 13:09
 * To change this template use File | Settings | File Templates.
 */
public class TerrainSpatial extends Spatial
{
    private ComponentMapper<PositionComponent> positionComponentComponentMapper;
    private ComponentMapper<AirFacilitatorComponent> airFacilitatorComponentComponentMapper;

    private Image terrainImage;


    @Override
    public void initalize()
    {
        positionComponentComponentMapper = world.getMapper(PositionComponent.class);
        airFacilitatorComponentComponentMapper = world.getMapper(AirFacilitatorComponent.class);

        PositionComponent position = positionComponentComponentMapper.get(owner);
        terrainImage = imageLoader.getImage(Entities.TERRAIN);
    }


    @Override
    public void render(Graphics g)
    {

        PositionComponent position = positionComponentComponentMapper.get(owner);

        rotateImage(terrainImage, position.getDirection());
        g.drawImage(terrainImage, 0, 0);
    }

}
