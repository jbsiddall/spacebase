package spacebase.world.spatials;

import com.artemis.ComponentMapper;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import spacebase.world.Entities;
import spacebase.world.components.PositionComponent;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 29/09/2012
 * Time: 23:33
 * To change this template use File | Settings | File Templates.
 */
public class AirGeneratorSpatial extends Spatial
{
    ComponentMapper<PositionComponent> positionComponentComponentMapper;

    Image image;

    @Override
    public void initalize() {
        positionComponentComponentMapper = world.getMapper(PositionComponent.class);

        image = imageLoader.getImage(Entities.AIR_GENERATOR);
    }

    @Override
    public void render(Graphics g) {

        g.drawImage(image, 0, 0);
    }
}
