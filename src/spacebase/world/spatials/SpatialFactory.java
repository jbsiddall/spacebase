package spacebase.world.spatials;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 29/09/2012
 * Time: 13:16
 * To change this template use File | Settings | File Templates.
 */
public interface SpatialFactory
{
    public AirGeneratorSpatial createAirGeneratorSpatial();

    public WorkerSpatial createPlayerSpatial();

    public TerrainSpatial createTerrainSpatial();

    public WallSpatial createWallSpatial();


}
