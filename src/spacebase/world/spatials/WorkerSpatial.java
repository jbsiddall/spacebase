package spacebase.world.spatials;

import com.artemis.ComponentMapper;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import spacebase.world.Direction;
import spacebase.world.Entities;
import spacebase.world.components.PositionComponent;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 30/09/2012
 * Time: 02:01
 * To change this template use File | Settings | File Templates.
 */
public class WorkerSpatial extends Spatial
{
    ComponentMapper<PositionComponent> positionComponentComponentMapper;

    private Image northImage;
    private Image eastImage;
    private Image southImage;
    private Image westImage;

    @Override
    public void initalize() {
        positionComponentComponentMapper = world.getMapper(PositionComponent.class);

        northImage = imageLoader.getImage(Entities.PLAYER + "_NORTH");
        eastImage = imageLoader.getImage(Entities.PLAYER + "_EAST");
        southImage = imageLoader.getImage(Entities.PLAYER + "_SOUTH");
        westImage = imageLoader.getImage(Entities.PLAYER + "_WEST");
    }

    @Override
    public void render(Graphics g) {
        PositionComponent positionComponent = positionComponentComponentMapper.get(owner);

        Image image = null;
        switch(positionComponent.getDirection())
        {
            case NORTH: image = northImage; break;
            case EAST: image = eastImage; break;
            case SOUTH: image = southImage; break;
            case WEST: image = westImage; break;
        }

        g.drawImage(image, 0, 0);
    }
}
