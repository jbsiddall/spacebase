package spacebase.world.spatials;

import com.artemis.ComponentMapper;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import spacebase.world.Entities;
import spacebase.world.components.PositionComponent;
import spacebase.world.util.SimpleRectangle;


public class WallSpatial extends Spatial
{
    private ComponentMapper<PositionComponent> positionComponentComponentMapper;
    private Image image;

    @Override
    public void initalize()
    {
        positionComponentComponentMapper = world.getMapper(PositionComponent.class);
        image = imageLoader.getImage(Entities.WALL);
    }

    @Override
    public void render(Graphics g)
    {
        PositionComponent position = positionComponentComponentMapper.get(owner);
        rotateImage(image, position.getDirection());
        g.drawImage(image, 0, 0);

    }
}
