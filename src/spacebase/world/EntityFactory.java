package spacebase.world;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.PlayerManager;
import com.artemis.managers.TagManager;
import spacebase.world.components.*;
import spacebase.world.entities.SpecificEntityFactory;


public class EntityFactory
{
    private ComponentFactory componentFactory;

    private World world;
    private String owner;

    private SpecificEntityFactory wallEntityFactory;


    public Entity createWall(int x, int y) { return createWall(x, y, Direction.NORTH); }
    public Entity createWall(int x, int y, Direction direction)
    {
        return wallEntityFactory.createEntity(x, y, direction, false);
    }

    public Entity createAirGenerator(int x, int y)
    {
        Entity entity = world.createEntity();

        PositionComponent positionComponent = componentFactory.createPositionComponent();
        positionComponent.setX(x);
        positionComponent.setY(y);
        positionComponent.setDirection(Direction.NORTH);
        entity.addComponent(positionComponent);

        AirFacilitatorComponent airFacilitatorComponent = componentFactory.createAirFacilitatorComponent();
        airFacilitatorComponent.setPorous(1);
        airFacilitatorComponent.setAirLevel(1.0);
        airFacilitatorComponent.setPriority(5);
        entity.addComponent(airFacilitatorComponent);

        AirGeneratorComponent airGeneratorComponent = componentFactory.createAirGeneratorComponent();
        airGeneratorComponent.setGenerationAmount(1.0);
        entity.addComponent(airGeneratorComponent);

        SpatialFormComponent spatialFormComponent = componentFactory.createSpatialFormComponent();
        spatialFormComponent.setSpatialFormFile(Entities.AIR_GENERATOR);
        entity.addComponent(spatialFormComponent);

        world.addEntity(entity);

        world.getManager(PlayerManager.class).setPlayer(entity, owner);
        world.getManager(GroupManager.class).add(entity, Entities.AIR_GENERATOR);
        world.getManager(GroupManager.class).add(entity, Entities.SOLID);

        return entity;

    }


    public Entity createTerrain(int x, int y) { return createTerrain(x, y, 0); }
    public Entity createTerrain(int x, int y, double airLevel)
    {
        Entity entity = world.createEntity();

        PositionComponent positionComponent = componentFactory.createPositionComponent();
        positionComponent.setX(x);
        positionComponent.setY(y);
        positionComponent.setDirection(Direction.NORTH);
        entity.addComponent(positionComponent);

        AirFacilitatorComponent airFacilitatorComponent = componentFactory.createAirFacilitatorComponent();
        airFacilitatorComponent.setPorous(1);
        airFacilitatorComponent.setAirLevel(airLevel);
        airFacilitatorComponent.setPriority(0);
        entity.addComponent(airFacilitatorComponent);

        SpatialFormComponent spatialFormComponent = componentFactory.createSpatialFormComponent();
        spatialFormComponent.setSpatialFormFile(Entities.TERRAIN);
        entity.addComponent(spatialFormComponent);

        entity.addToWorld();
        world.getManager(GroupManager.class).add(entity, Entities.TERRAIN);

        return entity;
    }


    public Entity createPlayer(int x, int y)
    {
        Entity entity = world.createEntity();

        PositionComponent positionComponent = componentFactory.createPositionComponent();
        positionComponent.setX(x);
        positionComponent.setY(y);
        positionComponent.setDirection(Direction.NORTH);
        entity.addComponent(positionComponent);

        SpatialFormComponent spatialFormComponent = componentFactory.createSpatialFormComponent();
        spatialFormComponent.setSpatialFormFile(Entities.PLAYER);
        entity.addComponent(spatialFormComponent);

        PlayerComponent playerComponent = componentFactory.createPlayerComponent();
        playerComponent.setHealth(100);
        entity.addComponent(playerComponent);

        entity.addToWorld();

        world.getManager(TagManager.class).register(Entities.PLAYER, entity);

        return entity;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setComponentFactory(ComponentFactory componentFactory) {
        this.componentFactory = componentFactory;
    }

    public void setWallEntityFactory(SpecificEntityFactory wallEntityFactory) {
        this.wallEntityFactory = wallEntityFactory;
    }
}
