package spacebase.world;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 30/09/2012
 * Time: 01:32
 * To change this template use File | Settings | File Templates.
 */
public class Entities
{
    // entities

    public static final String TERRAIN = "TERRAIN";

    public static final String WALL = "WALL";

    public static final String AIR_GENERATOR = "AIR_GENERATOR";

    public static final String MAP = "MAP";

    public static final String VIEW_PORT = "VIEW_PORT";

    public static final String PLAYER = "PLAYER";



    public static final String AIR = "AIR";

    public static final String SOLID = "SOLID";
}
