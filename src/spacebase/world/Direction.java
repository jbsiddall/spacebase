package spacebase.world;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 28/09/2012
 * Time: 16:20
 * To change this template use File | Settings | File Templates.
 */
public enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST
}
