package spacebase.world.systems;

import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import com.artemis.utils.ImmutableBag;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import spacebase.world.Entities;
import spacebase.world.EntityFactory;
import spacebase.world.components.PlayerComponent;
import spacebase.world.components.PositionComponent;
import spacebase.world.util.SimpleRectangle;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 29/09/2012
 * Time: 12:17
 * To change this template use File | Settings | File Templates.
 */
public class CommandGUISystem extends VoidEntitySystem
{
    private SimpleRectangle commandPanel;
    private GameContainer gameContainer;
    private EntityFactory entityFactory;

    private int selectedIndex;

    private String[] selections = new String[]{
            Entities.AIR_GENERATOR,
            Entities.WALL
    };

    @Override
    protected void processSystem()
    {
        Input input = gameContainer.getInput();

        if(input.isKeyPressed(Input.KEY_TAB))
        {
            selectedIndex++;
            selectedIndex %= selections.length;
        }

        if(input.isKeyPressed(Input.KEY_SPACE))
        {
            Entity player = world.getManager(TagManager.class).getEntity(Entities.PLAYER);
            ImmutableBag<Entity> entities = world.getManager(GroupManager.class).getEntities(selections[selectedIndex]);

            PositionComponent positionComponent = player.getComponent(PositionComponent.class);
            int nx = positionComponent.getX();
            int ny = positionComponent.getY();

            switch(positionComponent.getDirection())
            {
                case NORTH:
                    ny -= 1;
                    break;
                case SOUTH:
                    ny += 1;
                    break;
                case EAST:
                    nx += 1;
                    break;
                case WEST:
                    nx -= 1;
                    break;
            }

            boolean free = true;

            for(int i = 0; i < entities.size(); i++)
            {
                Entity e = entities.get(i);
                PositionComponent p = e.getComponent(PositionComponent.class);
                if(p.getX()==nx && p.getY()==ny)
                {
                    free = false;
                    e.deleteFromWorld();
                }
            }

            if(free)
            {
                String selected = selections[selectedIndex];

                if(selected.equals(Entities.AIR_GENERATOR))
                {
                    entityFactory.createAirGenerator(nx, ny);
                }
                else if(selected.equals(Entities.WALL))
                {
                    entityFactory.createWall(nx, ny);
                }
            }
        }
        render();
    }

    private void render()
    {
        Entity player = world.getManager(TagManager.class).getEntity(Entities.PLAYER);
        PlayerComponent playerComponent = player.getComponent(PlayerComponent.class);

        Graphics g = gameContainer.getGraphics();
        g.pushTransform();

        g.translate(commandPanel.getX(), commandPanel.getY());

        g.setColor(Color.green);
        g.fillRect(0, 0, commandPanel.getWidth(), commandPanel.getHeight());


        for(int i = 0; i < selections.length; i++)
        {
            if(i == selectedIndex)
                g.setColor(Color.red);
            else
                g.setColor(Color.black);
            g.drawString(selections[i], 100, i * 20 + 20);
        }
        g.setColor(Color.black);
        g.drawString(String.format("health: %d", playerComponent.getHealth()), 250, 20);

        g.popTransform();
    }


    public void setCommandPanel(SimpleRectangle commandPanel) {
        this.commandPanel = commandPanel;
    }

    public void setGameContainer(GameContainer gameContainer) {
        this.gameContainer = gameContainer;
    }

    public void setEntityFactory(EntityFactory entityFactory) {
        this.entityFactory = entityFactory;
    }

}
