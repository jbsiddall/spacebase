package spacebase.world.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.ImmutableBag;
import spacebase.world.Entities;
import spacebase.world.components.MovingComponent;
import spacebase.world.components.PositionComponent;


/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 30/09/2012
 * Time: 02:36
 * To change this template use File | Settings | File Templates.
 */
public class MovementSystem extends EntityProcessingSystem
{
    private ComponentMapper<PositionComponent> positionComponentComponentMapper;
    private ComponentMapper<MovingComponent> movingComponentComponentEvent;

    public MovementSystem() {
        super(Aspect.getAspectForAll(PositionComponent.class, MovingComponent.class));
    }

    @Override
    protected void initialize() {
        positionComponentComponentMapper = world.getMapper(PositionComponent.class);
        movingComponentComponentEvent = world.getMapper(MovingComponent.class);
    }

    @Override
    protected void process(Entity e) {
        PositionComponent positionComponent = positionComponentComponentMapper.get(e);
        MovingComponent movingComponent = movingComponentComponentEvent.get(e);

        int nx = positionComponent.getX();
        int ny = positionComponent.getY();

        switch(movingComponent.getDirection())
        {
            case NORTH:
                ny -= 1;
                break;
            case SOUTH:
                ny += 1;
                break;
            case EAST:
                nx += 1;
                break;
            case WEST:
                nx -= 1;
                break;
        }

        boolean available = true;
        ImmutableBag<Entity> solids = world.getManager(GroupManager.class).getEntities(Entities.SOLID);
        for(int i = 0; i < solids.size(); i++)
        {
            Entity possible = solids.get(i);
            PositionComponent position = positionComponentComponentMapper.get(possible);
            if(position.getX() == nx && position.getY() == ny)
            {
                available = false;
                break;
            }
        }

        if(available)
        {
            positionComponent.setX(nx);
            positionComponent.setY(ny);
        }

        positionComponent.setDirection(movingComponent.getDirection());
        e.removeComponent(movingComponent);
        e.addToWorld();


    }
}
