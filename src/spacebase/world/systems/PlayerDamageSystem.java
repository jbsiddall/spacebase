package spacebase.world.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.systems.IntervalEntityProcessingSystem;
import com.artemis.systems.IntervalEntitySystem;
import com.artemis.systems.VoidEntitySystem;
import com.artemis.utils.ImmutableBag;
import spacebase.world.Entities;
import spacebase.world.components.AirFacilitatorComponent;
import spacebase.world.components.PlayerComponent;
import spacebase.world.components.PositionComponent;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 23/04/2013
 * Time: 13:20
 * To change this template use File | Settings | File Templates.
 */
public class PlayerDamageSystem extends IntervalEntitySystem {

    public PlayerDamageSystem() {
        super(Aspect.getEmpty(), 500);
    }

    /**
     * Any implementing entity system must implement this method and the logic
     * to process the given entities of the system.
     *
     * @param entities the entities this system contains.
     */
    @Override
    protected void processEntities(ImmutableBag<Entity> entities) {
        processSystem();
    }

    protected void processSystem() {
        ComponentMapper<AirFacilitatorComponent> airFacilitatorComponentComponentMapper = ComponentMapper.getFor(AirFacilitatorComponent.class, world);
        ComponentMapper<PositionComponent> positionComponentComponentMapper = ComponentMapper.getFor(PositionComponent.class, world);
        ComponentMapper<PlayerComponent> playerComponentComponentMapper = ComponentMapper.getFor(PlayerComponent.class, world);
        Entity player = world.getManager(TagManager.class).getEntity(Entities.PLAYER);
        if(player == null) return;
        PositionComponent playerPosition = positionComponentComponentMapper.get(player);
        PlayerComponent playerComponent =  playerComponentComponentMapper.get(player);
        int playerX = playerPosition.getX();
        int playerY = playerPosition.getY();

        ImmutableBag<Entity> entities = world.getManager(GroupManager.class).getEntities(Entities.TERRAIN);
        for (int i = 0; i < entities.size(); i++) {
            Entity entity = entities.get(i);
            PositionComponent entityPosition = positionComponentComponentMapper.get(entity);
            int entityX = entityPosition.getX();
            int entityY = entityPosition.getY();

            if(playerX==entityX && playerY==entityY) {
                AirFacilitatorComponent airFacilitatorComponent = airFacilitatorComponentComponentMapper.get(entity);
                int health = playerComponent.getHealth();
                if(airFacilitatorComponent.getAirLevel() < 0.1) {
                    playerComponent.setHealth(health>0?health-5:0);
                    world.changedEntity(player);
                } else if(airFacilitatorComponent.getAirLevel() > 0.7) {
                    playerComponent.setHealth(health<100?health+1:100);
                }

                if(playerComponent.getHealth() == 0) {
                    System.exit(0);
                }
            }
        }


    }
}
