package spacebase.world.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import spacebase.world.Direction;
import spacebase.world.components.ComponentFactory;
import spacebase.world.components.MovingComponent;
import spacebase.world.components.PlayerComponent;
import spacebase.world.components.PositionComponent;


public class PlayerKeyBoardControlSystem extends EntityProcessingSystem
{
    private GameContainer gameContainer;
    private ComponentFactory componentFactory;

    private ComponentMapper<PositionComponent> positionComponentComponentMapper;
    private ComponentMapper<MovingComponent> movingComponentComponentMapper;
    private ComponentMapper<PlayerComponent> playerComponentComponentMapper;

    private int upKey = Input.KEY_W;
    private int downKey = Input.KEY_S;
    private int leftKey = Input.KEY_A;
    private int rightKey = Input.KEY_D;

    public PlayerKeyBoardControlSystem() {
        super(Aspect.getAspectForAll(PositionComponent.class, PlayerComponent.class));
    }

    @Override
    protected void initialize() {
        positionComponentComponentMapper = world.getMapper(PositionComponent.class);
        movingComponentComponentMapper = world.getMapper(MovingComponent.class);
        playerComponentComponentMapper = world.getMapper(PlayerComponent.class);
    }

    @Override
    protected void process(Entity e) {

        if(movingComponentComponentMapper.has(e))
            return;

        Input input = gameContainer.getInput();

        Direction direction = null;
               input.enableKeyRepeat();
        if(input.isKeyPressed(upKey) || input.isKeyDown(upKey))
            direction = Direction.NORTH;
        if(input.isKeyPressed(downKey) || input.isKeyDown(downKey))
            direction = Direction.SOUTH;
        if(input.isKeyPressed(Input.KEY_A))
            direction = Direction.WEST;
        if(input.isKeyPressed(Input.KEY_D))
            direction = Direction.EAST;


        if(direction == null)
            return;

        PositionComponent positionComponent = positionComponentComponentMapper.get(e);

        MovingComponent movingComponent = componentFactory.createMovingComponent();
        movingComponent.setDirection(direction);
        e.addComponent(movingComponent);
        e.addToWorld();
    }


    public void setGameContainer(GameContainer gameContainer) {
        this.gameContainer = gameContainer;
    }

    public void setComponentFactory(ComponentFactory componentFactory) {
        this.componentFactory = componentFactory;
    }
}
