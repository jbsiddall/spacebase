package spacebase.world.systems;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 28/09/2012
 * Time: 22:27
 * To change this template use File | Settings | File Templates.
 */
public interface SystemFactory
{
    public AirGeneratorSystem getAirGeneratorSystem();

    public AirMovementSystem getAirMovementSystem();

    public AirRenderSystem getAirRenderSystem();

    public CommandGUISystem getCommandGUISystem();

    public MovementSystem getMovementSystem();

    public PlayerDamageSystem getPlayerDamageSystem();

    public PlayerKeyBoardControlSystem getPlayerKeyBoardControlSystem();

    public RenderSystem getRenderSystem();

    public ViewPortUpdaterSystem getViewPortUpdateSystem();

}
