package spacebase.world.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.managers.TagManager;
import com.artemis.utils.ImmutableBag;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import spacebase.world.Entities;
import spacebase.world.components.MapComponent;
import spacebase.world.components.PositionComponent;
import spacebase.world.components.SpatialFormComponent;
import spacebase.world.components.ViewPortComponent;
import spacebase.world.spatials.*;
import spacebase.world.util.SimpleRectangle;

import java.util.HashMap;
import java.util.Map;


public class RenderSystem extends EntitySystem
{
    private ComponentMapper<SpatialFormComponent> spatialFormComponentComponentMapper;
    private ComponentMapper<MapComponent> mapComponentComponentMapper;
    private ComponentMapper<ViewPortComponent> viewPortComponentComponentMapper;
    private ComponentMapper<PositionComponent> positionComponentComponentMapper;

    private SpatialFactory spatialFactory;

    private Map<Integer, Map<Entity, Spatial>> spatials;
    private Map<Entity, Integer> entityDepths;

    private GameContainer gameContainer;
    private ViewPortComponent viewPort;

    private SimpleRectangle gamePanel;

    public RenderSystem(SimpleRectangle gamePanel)
    {
        super(Aspect.getAspectForAll(SpatialFormComponent.class));

        this.gamePanel = gamePanel;

        spatials = new HashMap<Integer, Map<Entity, Spatial>>();
        for(int depth = Spatial.DEPTH_MIN; depth <= Spatial.DEPTH_MAX; depth++)
            spatials.put(depth, new HashMap<Entity, Spatial>());

        entityDepths = new HashMap<Entity, Integer>();
    }

    @Override
    protected void initialize()
    {
        spatialFormComponentComponentMapper = world.getMapper(SpatialFormComponent.class);
        mapComponentComponentMapper = world.getMapper(MapComponent.class);
        viewPortComponentComponentMapper = world.getMapper(ViewPortComponent.class);
        positionComponentComponentMapper = world.getMapper(PositionComponent.class);
    }

    @Override
    protected void inserted(Entity e)
    {
        Spatial spatial = generateSpatial(e);
        spatial.initalize();
        spatials.get(spatial.getDepth()).put(e, spatial);
        entityDepths.put(e, spatial.getDepth());
    }

    @Override
    protected void removed(Entity e) {
        int depth = entityDepths.get(e);
        spatials.get(depth).remove(e);
        entityDepths.remove(e);
    }

    @Override
    protected void begin()
    {
        TagManager tagManager = world.getManager(TagManager.class);
        viewPort = viewPortComponentComponentMapper.get(tagManager.getEntity("VIEW_PORT"));

        Graphics g = gameContainer.getGraphics();


        //changes to graphics
        g.setColor(Color.black);
        g.fillRect(gamePanel.getX(), gamePanel.getY(), gamePanel.getWidth(), gamePanel.getHeight());
        g.setClip(gamePanel.getX(), gamePanel.getY(), gamePanel.getWidth(), gamePanel.getHeight());

        float translateX = -viewPort.getMicroX();
        float translateY = -viewPort.getMicroY();

        float scaleWidth = gamePanel.getWidth() / viewPort.getMicroWidth();
        float scaleHeight = gamePanel.getHeight() / viewPort.getMicroHeight();

        g.pushTransform();

        g.translate(gamePanel.getX(), gamePanel.getY());
        g.scale(scaleWidth, scaleHeight);
        g.translate(translateX, translateY);
    }


    @Override
    protected void processEntities(ImmutableBag<Entity> entityImmutableBag) {

        for(int depth = Spatial.DEPTH_MAX; depth >= Spatial.DEPTH_MIN; depth--)
        {
            for(Entity e : spatials.get(depth).keySet())
                process(e, spatials.get(depth).get(e));
        }

    }

    private void process(Entity e, Spatial s)
    {
        Graphics g = gameContainer.getGraphics();
        PositionComponent position = positionComponentComponentMapper.get(e);

        g.pushTransform();
        g.translate(position.getX(), position.getY());
        s.render(g);
        g.popTransform();

    }

    @Override
    protected void end()
    {
        gameContainer.getGraphics().popTransform();
        gameContainer.getGraphics().clearClip();
        viewPort = null;
    }

    @Override
    protected boolean checkProcessing() {
        return true;
    }

    public void setGameContainer(GameContainer gameContainer) {
        this.gameContainer = gameContainer;
    }

    public void setSpatialFactory(SpatialFactory spatialFactory) {
        this.spatialFactory = spatialFactory;
    }

    private Spatial generateSpatial(Entity e)
    {
        String spatialName = spatialFormComponentComponentMapper.get(e).getSpatialFormFile();

        if(spatialName.equals(Entities.WALL))
        {
            WallSpatial spatial = spatialFactory.createWallSpatial();
            spatial.setOwner(e);
            return spatial;
        }
        else if(spatialName.equals(Entities.TERRAIN))
        {
            TerrainSpatial spatial = spatialFactory.createTerrainSpatial();
            spatial.setOwner(e);
            return spatial;
        }
        else if(spatialName.equals(Entities.AIR_GENERATOR))
        {
            AirGeneratorSpatial spatial = spatialFactory.createAirGeneratorSpatial();
            spatial.setOwner(e);
            return spatial;
        }
        else if(spatialName.equals(Entities.PLAYER))
        {
            WorkerSpatial spatial = spatialFactory.createPlayerSpatial();
            spatial.setOwner(e);
            return spatial;
        }


        throw new IllegalStateException("unknown spatial name: " + spatialName);
    }

}
