package spacebase.world.systems;

import com.artemis.Aspect;
import com.artemis.ComponentManager;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.systems.IntervalEntityProcessingSystem;
import spacebase.world.components.AirFacilitatorComponent;
import spacebase.world.components.AirGeneratorComponent;



public class AirGeneratorSystem extends IntervalEntityProcessingSystem
{
    private ComponentMapper<AirFacilitatorComponent> airFacilitatorComponentComponentMapper;
    private ComponentMapper<AirGeneratorComponent> airGeneratorComponentComponentMapper;

    public AirGeneratorSystem(long interval) {
        super(Aspect.getAspectForAll(AirFacilitatorComponent.class, AirGeneratorComponent.class), interval);
    }

    @Override
    protected void initialize() {
        super.initialize();
        airFacilitatorComponentComponentMapper = world.getMapper(AirFacilitatorComponent.class);
        airGeneratorComponentComponentMapper = world.getMapper(AirGeneratorComponent.class);

    }

    @Override
    protected void process(Entity e)
    {
        AirFacilitatorComponent airFacilitatorComponent = airFacilitatorComponentComponentMapper.get(e);
        AirGeneratorComponent airGeneratorComponent = airGeneratorComponentComponentMapper.get(e);

        airFacilitatorComponent.addAir(airGeneratorComponent.getGenerationAmount());
    }
}
