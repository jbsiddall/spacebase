package spacebase.world.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.systems.IntervalEntitySystem;
import com.artemis.utils.ImmutableBag;
import spacebase.world.components.AirFacilitatorComponent;
import spacebase.world.components.MapComponent;
import spacebase.world.components.PositionComponent;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 29/09/2012
 * Time: 21:41
 * To change this template use File | Settings | File Templates.
 */
public class AirMovementSystem extends IntervalEntitySystem
{
    private ComponentMapper<PositionComponent> positionComponentComponentMapper;
    private ComponentMapper<AirFacilitatorComponent> airFacilitatorComponentComponentMapper;

    private ComponentMapper<MapComponent> mapComponentComponentMapper;

    private Entity map;

    private double minimumTransferAmount;
    private double minimumAirLevelToClear;

    public AirMovementSystem(int interval)
    {
        super(Aspect.getAspectForAll(PositionComponent.class, AirFacilitatorComponent.class), interval);
    }

    @Override
    protected void initialize()
    {
        positionComponentComponentMapper = world.getMapper(PositionComponent.class);
        airFacilitatorComponentComponentMapper = world.getMapper(AirFacilitatorComponent.class);
        mapComponentComponentMapper = world.getMapper(MapComponent.class);
    }

    @Override
    protected void processEntities(ImmutableBag<Entity> entityImmutableBag)
    {
        MapComponent map = mapComponentComponentMapper.get(world.getManager(TagManager.class).getEntity("MAP"));

        AirFacilitatorComponent[][] airComponents = new AirFacilitatorComponent[map.getWidth()][map.getHeight()];

        for(int index = 0; index < entityImmutableBag.size(); index++)
        {
            Entity e = entityImmutableBag.get(index);
            PositionComponent position = positionComponentComponentMapper.get(e);
            AirFacilitatorComponent airFacilitator = airFacilitatorComponentComponentMapper.get(e);
            if(airComponents[position.getX()][position.getY()] != null)
            {
                AirFacilitatorComponent competition = airComponents[position.getX()][position.getY()];
                if(airFacilitator.getPriority() >= competition.getPriority())
                {
                    airComponents[position.getX()][position.getY()] = airFacilitator;
                }
                else
                {
                    airComponents[position.getX()][position.getY()] = competition;
                }

            }
            else
            {
                airComponents[position.getX()][position.getY()] = airFacilitator;
            }

        }

        for(int x = 0; x < map.getWidth(); x++)
        {
            for(int y = 0; y < map.getHeight(); y++)
            {
                AirFacilitatorComponent centre = airComponents[x][y];

                if(centre == null)
                    continue;;

                for(int[] ncoord : new int[][]{{x-1, y}, {x+1, y}, {x, y-1}, {x, y+1} })
                {
                    int nx = ncoord[0];
                    int ny = ncoord[1];

                    if(nx < 0) continue;
                    if(nx >= map.getWidth()) continue;

                    if(ny < 0) continue;
                    if(ny >= map.getHeight()) continue;

                    AirFacilitatorComponent adjacent = airComponents[nx][ny];

                    if(adjacent == null) continue;

                    double transferRate = centre.getPorous() * adjacent.getPorous();
                    double difference = centre.getAirLevel() - adjacent.getAirLevel();

                    double transferAmount = difference/2.0 * transferRate;

                    if(transferAmount < minimumTransferAmount)
                        continue;

                    centre.addAir(-transferAmount);
                    adjacent.addAir(transferAmount);

                    if(centre.getAirLevel() < minimumAirLevelToClear)
                        centre.setAirLevel(0.0);

                    if(adjacent.getAirLevel() < minimumAirLevelToClear)
                        adjacent.setAirLevel(0.0);


                }
            }
        }

    }


    public void setMinimumTransferAmount(double minimumTransferAmount) {
        this.minimumTransferAmount = minimumTransferAmount;
    }

    public void setMinimumAirLevelToClear(double minimumAirLevelToClear) {
        this.minimumAirLevelToClear = minimumAirLevelToClear;
    }
}
