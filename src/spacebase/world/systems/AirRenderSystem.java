package spacebase.world.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.ImmutableBag;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import spacebase.world.Entities;
import spacebase.world.ImageLoader;
import spacebase.world.components.*;
import spacebase.world.spatials.Spatial;
import spacebase.world.spatials.SpatialFactory;
import spacebase.world.spatials.TerrainSpatial;
import spacebase.world.spatials.WallSpatial;
import spacebase.world.util.SimpleRectangle;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 29/09/2012
 * Time: 21:19
 * To change this template use File | Settings | File Templates.
 */
public class AirRenderSystem extends EntityProcessingSystem
{
    private ComponentMapper<MapComponent> mapComponentComponentMapper;
    private ComponentMapper<ViewPortComponent> viewPortComponentComponentMapper;
    private ComponentMapper<PositionComponent> positionComponentComponentMapper;
    private ComponentMapper<AirFacilitatorComponent> airFacilitatorComponentComponentMapper;

    private GameContainer gameContainer;
    private ViewPortComponent viewPort;

    private SimpleRectangle gamePanel;

    ImageLoader imageLoader;
    private Image airImage;

    public AirRenderSystem(SimpleRectangle gamePanel)
    {
        super(Aspect.getAspectForAll(PositionComponent.class, AirFacilitatorComponent.class));
        this.gamePanel = gamePanel;

    }

    @Override
    protected void initialize()
    {
        mapComponentComponentMapper = world.getMapper(MapComponent.class);
        viewPortComponentComponentMapper = world.getMapper(ViewPortComponent.class);
        positionComponentComponentMapper = world.getMapper(PositionComponent.class);
        airFacilitatorComponentComponentMapper = world.getMapper(AirFacilitatorComponent.class);

        airImage = imageLoader.getImage(Entities.AIR);
    }

    @Override
    protected void begin()
    {
        TagManager tagManager = world.getManager(TagManager.class);
        viewPort = viewPortComponentComponentMapper.get(tagManager.getEntity("VIEW_PORT"));

        Graphics g = gameContainer.getGraphics();
        g.pushTransform();

        g.setClip(gamePanel.getX(), gamePanel.getY(), gamePanel.getWidth(), gamePanel.getHeight());

        float translateX = -viewPort.getMicroX();
        float translateY = -viewPort.getMicroY();

        float scaleWidth = gamePanel.getWidth() / viewPort.getMicroWidth();
        float scaleHeight = gamePanel.getHeight() / viewPort.getMicroHeight();


        g.translate(gamePanel.getX(), gamePanel.getY());
        g.scale(scaleWidth, scaleHeight);
        g.translate(translateX, translateY);
    }

    @Override
    public void process(Entity e)
    {
        Graphics g = gameContainer.getGraphics();
        PositionComponent position = positionComponentComponentMapper.get(e);
        AirFacilitatorComponent airFacilitatorComponent = airFacilitatorComponentComponentMapper.get(e);

        g.pushTransform();
        g.translate(position.getX(), position.getY());
        processEntity(g, position, airFacilitatorComponent);
        g.popTransform();

    }

    private void processEntity(Graphics g,
                               PositionComponent positionComponent,
                               AirFacilitatorComponent airFacilitatorComponent)
    {
        if(airFacilitatorComponent.getAirLevel() > 0.001)
        {
            airImage.setAlpha((float)airFacilitatorComponent.getAirLevel());
            g.drawImage(airImage, 0, 0);
        }
    }


    @Override
    protected void end()
    {
        gameContainer.getGraphics().popTransform();
        gameContainer.getGraphics().clearClip();
        viewPort = null;
    }

    @Override
    protected boolean checkProcessing() {
        return true;
    }

    public void setGameContainer(GameContainer gameContainer) {
        this.gameContainer = gameContainer;
    }

    public void setImageLoader(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }
}
