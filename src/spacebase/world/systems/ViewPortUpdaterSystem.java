package spacebase.world.systems;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import spacebase.world.Entities;
import spacebase.world.components.MapComponent;
import spacebase.world.components.PositionComponent;
import spacebase.world.components.ViewPortComponent;
import spacebase.world.util.SimpleRectangle;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 28/09/2012
 * Time: 21:36
 * To change this template use File | Settings | File Templates.
 */
public class ViewPortUpdaterSystem extends VoidEntitySystem
{
    private GameContainer gameContainer;

    private ComponentMapper<MapComponent> mapComponentComponentMapper;
    private ComponentMapper<ViewPortComponent> viewPortComponentComponentMapper;
    private ComponentMapper<PositionComponent> positionComponentComponentMapper;

    private SimpleRectangle minimumBlockCoverage;
    private SimpleRectangle maximumBlockCoverage;

    private double cameraSensitivity;

    @Override
    protected void initialize()
    {
        mapComponentComponentMapper = world.getMapper(MapComponent.class);
        viewPortComponentComponentMapper = world.getMapper(ViewPortComponent.class);
        positionComponentComponentMapper = world.getMapper(PositionComponent.class);
    }

    @Override
    protected void processSystem()
    {

        Entity viewPortEntity = world.getManager(TagManager.class).getEntity(Entities.VIEW_PORT);
        ViewPortComponent viewPort = viewPortComponentComponentMapper.get(viewPortEntity);

        Entity mapEntity = world.getManager(TagManager.class).getEntity(Entities.MAP);
        MapComponent map = mapComponentComponentMapper.get(mapEntity);

        Entity player = world.getManager(TagManager.class).getEntity(Entities.PLAYER);
        PositionComponent playerPosition = positionComponentComponentMapper.get(player);


        int px = playerPosition.getX();
        int py = playerPosition.getY();

        viewPort.setMicroX(px - (float)Math.ceil(viewPort.getMicroWidth()/2)+1);
        viewPort.setMicroY(py - (float)Math.ceil(viewPort.getMicroHeight()/2)+1);


        if(viewPort.getMicroWidth() > map.getWidth())
            viewPort.setMicroWidth(map.getWidth());

        if(viewPort.getMicroHeight() > map.getHeight())
            viewPort.setMicroHeight(map.getHeight());


        if(viewPort.getMicroWidth() > maximumBlockCoverage.getWidth())
            viewPort.setMicroWidth(maximumBlockCoverage.getWidth());

        if(viewPort.getMicroHeight() > maximumBlockCoverage.getHeight())
            viewPort.setMicroHeight(maximumBlockCoverage.getHeight());


        if(viewPort.getMicroWidth() < minimumBlockCoverage.getWidth())
            viewPort.setMicroWidth(minimumBlockCoverage.getWidth());

        if(viewPort.getMicroHeight() < minimumBlockCoverage.getHeight())
            viewPort.setMicroHeight(minimumBlockCoverage.getHeight());


        if(viewPort.getMicroX() < 0)
            viewPort.setMicroX(0);

        if(viewPort.getMicroY() < 0)
            viewPort.setMicroY(0);


        if(viewPort.getMicroX() + viewPort.getMicroWidth() > map.getWidth())
            viewPort.setMicroX(map.getWidth() - viewPort.getMicroWidth());

        if(viewPort.getMicroY() + viewPort.getMicroHeight() > map.getHeight())
            viewPort.setMicroY(map.getHeight() - viewPort.getMicroHeight());


    }

//    @Override
//    protected void processSystem()
//    {
//
//        Entity viewPortEntity = world.getManager(TagManager.class).getEntity("VIEW_PORT");
//        ViewPortComponent viewPort = viewPortComponentComponentMapper.get(viewPortEntity);
//
//        Entity mapEntity = world.getManager(TagManager.class).getEntity("MAP");
//        MapComponent map = mapComponentComponentMapper.get(mapEntity);
//
//        Input input = gameContainer.getInput();
//
//        float dx = 0;
//        float dy = 0;
//        float dZoom = 0;
//
//
//        if(input.isKeyDown(Input.KEY_W))
//            dy -= cameraSensitivity;
//        if(input.isKeyDown(Input.KEY_S))
//            dy += cameraSensitivity;
//        if(input.isKeyDown(Input.KEY_A))
//            dx -= cameraSensitivity;
//        if(input.isKeyDown(Input.KEY_D))
//            dx += cameraSensitivity;
//        if(input.isKeyDown(Input.KEY_Q))
//            dZoom += cameraSensitivity;
//        if(input.isKeyDown(Input.KEY_Z))
//            dZoom -= cameraSensitivity;
//
//
//        viewPort.zoom(dZoom);
//        viewPort.move(dx, dy);
//
//
//        float left = viewPort.getMicroX();
//        float top = viewPort.getMicroY();
//        float right = viewPort.getMicroX() + viewPort.getMicroWidth();
//        float bottom = viewPort.getMicroY() + viewPort.getMicroHeight();
//
//
//        if(viewPort.getMicroWidth() > map.getWidth())
//            viewPort.setMicroWidth(map.getWidth());
//
//        if(viewPort.getMicroHeight() > map.getHeight())
//            viewPort.setMicroHeight(map.getHeight());
//
//
//        if(viewPort.getMicroWidth() > maximumBlockCoverage.getWidth())
//            viewPort.setMicroWidth(maximumBlockCoverage.getWidth());
//
//        if(viewPort.getMicroHeight() > maximumBlockCoverage.getHeight())
//            viewPort.setMicroHeight(maximumBlockCoverage.getHeight());
//
//
//        if(viewPort.getMicroWidth() < minimumBlockCoverage.getWidth())
//            viewPort.setMicroWidth(minimumBlockCoverage.getWidth());
//
//        if(viewPort.getMicroHeight() < minimumBlockCoverage.getHeight())
//            viewPort.setMicroHeight(minimumBlockCoverage.getHeight());
//
//
//        if(viewPort.getMicroX() < 0)
//            viewPort.setMicroX(0);
//
//        if(viewPort.getMicroY() < 0)
//            viewPort.setMicroY(0);
//
//
//        if(viewPort.getMicroX() + viewPort.getMicroWidth() > map.getWidth())
//            viewPort.setMicroX(map.getWidth() - viewPort.getMicroWidth());
//
//        if(viewPort.getMicroY() + viewPort.getMicroHeight() > map.getHeight())
//            viewPort.setMicroY(map.getHeight() - viewPort.getMicroHeight());
//
//
//    }


    public void setGameContainer(GameContainer gameContainer) {
        this.gameContainer = gameContainer;
    }

    public void setMinimumBlockCoverage(SimpleRectangle minimumBlockCoverage) {
        this.minimumBlockCoverage = minimumBlockCoverage;
    }

    public void setMaximumBlockCoverage(SimpleRectangle maximumBlockCoverage) {
        this.maximumBlockCoverage = maximumBlockCoverage;
    }

    public void setCameraSensitivity(double cameraSensitivity) {
        this.cameraSensitivity = cameraSensitivity;
    }
}
