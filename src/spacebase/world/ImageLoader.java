package spacebase.world;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 29/09/2012
 * Time: 18:11
 * To change this template use File | Settings | File Templates.
 */
public class ImageLoader
{
    private Image terrain;
    private Image wall;
    private Image air;
    private Image airGenerator;
    private Image playerNorth;
    private Image playerEast;
    private Image playerSouth;
    private Image playerWest;


    public void initialize()
    {

        SpriteSheet terrainSheet;
        try
        {
            terrainSheet = new SpriteSheet(new Image("res/terrainsheet.png"), 128, 128);

        }
        catch(SlickException e)
        {
            throw new RuntimeException(e);
        }

        terrain = process(terrainSheet.getSprite(8, 6));
        wall = process(terrainSheet.getSprite(6, 1));
        air = process(terrainSheet.getSprite(3, 4));
        airGenerator = process(terrainSheet.getSprite(1, 2));

        playerNorth = process("res/worker_north.png");
        playerEast = process("res/worker_east.png");
        playerSouth = process("res/worker_south.png");
        playerWest = process("res/worker_west.png");
    }


    public Image getImage(String name)
    {
        if(name.equals(Entities.TERRAIN))
        {
            return copy(terrain);
        }
        else if(name.equals(Entities.WALL))
        {
            return copy(wall);
        }
        else if(name.equals(Entities.AIR))
        {
            return copy(air);
        }
        else if(name.equals(Entities.AIR_GENERATOR))
        {
            return copy(airGenerator);
        }
        else if(name.equals(Entities.PLAYER + "_NORTH"))
        {
            return copy(playerNorth);
        }
        else if(name.equals(Entities.PLAYER + "_EAST"))
        {
            return copy(playerEast);
        }
        else if(name.equals(Entities.PLAYER + "_SOUTH"))
        {
            return copy(playerSouth);
        }
        else if(name.equals(Entities.PLAYER + "_WEST"))
        {
            return copy(playerWest);
        }

        throw new IllegalArgumentException("unknown image name: " + name);
    }

    private Image copy(Image image)
    {
        Image newImage = image.copy();
        newImage.setCenterOfRotation(image.getCenterOfRotationX(), image.getCenterOfRotationY());
        return newImage;
    }


    private Image process(String imageLocation)
    {
        try{
            Image image = new Image(imageLocation);
            return process(image);
        }
        catch(SlickException e)
        {
            throw new RuntimeException(e);
        }
    }

    private Image process(Image image)
    {
        image = image.getScaledCopy(1, 1);
        image.setCenterOfRotation(0.5f, 0.5f);
        return image;
    }


}
