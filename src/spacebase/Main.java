package spacebase;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * User: joseph
 * Date: 27/09/2012
 * Time: 23:59
 * To change this template use File | Settings | File Templates.
 */
public class Main
{

    public static void main(String[] args) throws SlickException
    {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        AppGameContainer container = (AppGameContainer)context.getBean("appGameContainer");
        container.start();
    }

}
