package spacebase;

import com.artemis.*;
import com.artemis.managers.GroupManager;
import com.artemis.managers.PlayerManager;
import com.artemis.managers.TagManager;
import org.newdawn.slick.*;
import spacebase.world.Direction;
import spacebase.world.EntityFactory;
import spacebase.world.ImageLoader;
import spacebase.world.MapGenerator;
import spacebase.world.components.MapComponent;
import spacebase.world.components.ViewPortComponent;
import spacebase.world.systems.RenderSystem;
import spacebase.world.systems.SystemFactory;
import spacebase.world.systems.ViewPortUpdaterSystem;

public class SpaceBaseGame extends BasicGame
{
    private World world;

    private SystemFactory systemFactory;

    private ImageLoader imageLoader;

    private MapGenerator mapGenerator;


    public SpaceBaseGame(String title) {
        super(title);
    }

    @Override
    public void init(GameContainer container) throws SlickException
    {
        imageLoader.initialize();

        world.setManager(new PlayerManager());
        world.setManager(new GroupManager());
        world.setManager(new TagManager());

        world.setSystem(systemFactory.getAirGeneratorSystem());
        world.setSystem(systemFactory.getAirMovementSystem());
        world.setSystem(systemFactory.getPlayerKeyBoardControlSystem());
        world.setSystem(systemFactory.getMovementSystem());
        world.setSystem(systemFactory.getPlayerDamageSystem());

        world.setSystem(systemFactory.getAirRenderSystem(), true);
        world.setSystem(systemFactory.getRenderSystem(), true);
        world.setSystem(systemFactory.getViewPortUpdateSystem(), true);
        world.setSystem(systemFactory.getCommandGUISystem(), true);

        world.initialize();

        mapGenerator.generate(21);


    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException
    {
        world.setDelta(delta);
        world.process();
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException
    {
        g.setBackground(Color.pink);

        systemFactory.getViewPortUpdateSystem().process();
        systemFactory.getRenderSystem().process();
        systemFactory.getAirRenderSystem().process();
        systemFactory.getCommandGUISystem().process();
    }


    public void setWorld(World world) {
        this.world = world;
    }

    public void setSystemFactory(SystemFactory systemFactory) {
        this.systemFactory = systemFactory;
    }

    public void setImageLoader(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    public void setMapGenerator(MapGenerator mapGenerator) {
        this.mapGenerator = mapGenerator;
    }
}
